import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import LoginForm from './components/LoginForm';
import Router from './Router';

class App extends Component {
  componentWillMount() {
    const config = {
      apiKey: "AIzaSyCeTEBXahKUpU-QXAZuWvWVApYgpBGV8kc",
      authDomain: "manager-7e21d.firebaseapp.com",
      databaseURL: "https://manager-7e21d.firebaseio.com",
      projectId: "manager-7e21d",
      storageBucket: "",
      messagingSenderId: "17309588730"
    };
    firebase.initializeApp(config);
  }
  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
